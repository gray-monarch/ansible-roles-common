gray-monarch.common/shell-command
=========

Run a single shell command (or pipe) useful for interjecting role-based plays

###### Purpose:

  Ansible does not have an easy way to interject commands between roles, the idea of this role is to easily be able to run arbitrary shell commands between roles, where it is more appropriate to write that command in the playbook than bake it into the end or beginning of the adjacent roles.

###### Features:
  - Allows user to change executable with `gm_shell_exec_path` (e.g. `/bin/python3`)
  - Allows user to `chdir` to a different location before executing
  - Allows user to leverage `creates: / removes:` to maintain idempotency
  - Allows user to only run command on a single host (with `gm_shell_run_once: true`)
  - Allows user to delegate the shell command to a remote host (with `gm_shell_delegate_to: <host>`, can be `localhost` )
  - Allows user to register the output of the command into the `gm_shell_register_dict` dictionary by key name `gm_shell_register_key`

###### Notes:
  - `register:` with a dynamic variable is NOT possible, so instead i've made a separate dictionary, `gm_shell_register_dict` that will contain the normal register output of the shell command, and is accessed by `gm_shell_register_key[gm_shell_register_key][attribute]`

      Example:

          # Normal debug output for a register might look like:
          - debug: var=foo_output.stdout_lines

          # The way this role registers output is accessed as follows:
          - debug: var=gm_shell_register_dict.foo_output.stdout_lines

Requirements
------------

None

Role Variables
--------------

| Variable                | Description                                                                             | Default                |
|-------------------------|-----------------------------------------------------------------------------------------|------------------------|
| `gm_shell_command`      | The command to be run, can be a chain (e.g. `echo foo && echo bar` )                    |                        |
| `gm_shell_dir`          | Optional directory to change to before running `gm_shell_command`                       | omitted if `undefined` |
| `gm_shell_delegate_to`  | Optional host to run `gm_shell_command` on, can be `localhost` to mimic `local_action:` | omitted if `undefined` |
| `gm_shell_register_key` | Optional key to reference in `gm_shell_register_dict` that contains normal `register: ` output | omitted if `undefined` |
| `gm_shell_removes`      | Optional path of file this command removes (checks if not exists).<br />See: <code>ansible-doc -s shell &#124; grep "creates"</code> | omitted if `undefined` |
| `gm_shell_creates` | Optional path of file this command creates (checks if file exists).<br />See: <code>ansible-doc -s shell &#124; grep "removes"</code> | omitted if `undefined` |
| `gm_shell_exec_path` | Optional path of executable binary. e.g. `/bin/sh` or `/bin/python` | omitted if `undefined` |

Dependencies
------------

None

Example Playbooks
----------------

    - hosts: servers

      roles:
         - role: gray-monarch.common/shell-command
           gm_shell_command: >
             uptime && uname -a
           gm_shell_dir: "{{ ansible_env.HOME }}/some/path"
           gm_shell_register_key: uptime_output

      post_tasks:
        - debug: var=gm_shell_register_dict.uptime_output
        - debug: var=gm_shell_register_dict.uptime_output.stdout_lines
        - debug: var=gm_shell_register_dict.uptime_output.stdout

Using an alternative executable binary:

    - hosts: servers

      roles:
        - role: gray-monarch.common/shell-command
          gm_shell_command: >
            print('foo')
          gm_shell_exec_path: /bin/python3

License
-------

Apache 2.0

Author Information
------------------

Michael Goodwin <mike@contactvelocity.com>
