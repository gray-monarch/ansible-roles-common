common/install-swapfile
=========

Create, configure, and install a swap file. Features include:

-   Size is customizable by variable
-   Location is customizable by variable
-   Multiplier is customizable by variable (default swap size is RAM x this)
-   Mandatory free percentage is customizable (15% default)

Requirements
------------

None

Role Variables
--------------
| Variable                         | Default                | Use case                                              |
|----------------------------------|------------------------|-------------------------------------------------------|
| `gm_swapfile_location`           | `/swapfile`            | Absolute path of swap file                            |
| `gm_swapfile_size_mb`            | Total RAM * Multiplier | Size of swapfile in Megabytes                         |
| `gm_swapfile_multiplier`         | `2`                    | Factor to multiply RAM by to get swapfile default     |
| `gm_swapfile_space_free_percent` | `15`                   | Percent of free space needed to allow play to succeed |

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

##### requirements.xml
    - src: git@gitlab.com:gray-monarch/ansible-roles-common.git
      scm: git
      name: gray-monarch.common/install-swapfile


##### Top-level playbook:
    - hosts: servers
      roles:
         - role: gray-monarch.common/install-swapfile
           #gm_swapfile_size_mb: 2048 # 2GB swap override
           #gm_swapfile_location: /swapfile
           #gm_swapfile_multiplier: 1.5
           #gm_swapfile_space_free_percent: 25

License
-------

Apache 2

Author Information
------------------

Michael Goodwin <mike@contactvelocity.com>
